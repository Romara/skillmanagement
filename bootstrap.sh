#!/usr/bin/env bash

# python repository add
add-apt-repository ppa:fkrull/deadsnakes -y
apt-get update

# Frontend nodejs
# curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
# apt-get install -y nodejs
# npm install npm@latest -g
# cd /vagrant/cvart-ui/
# npm install

# Python
apt-get install -y python-pip python3-pip python3.5-dev

# Postgre db
apt-get install -y libpq-dev postgresql postgresql-contrib postgresql-server-dev-all

pip install --upgrade pip
pip install --upgrade virtualenv
pip install --upgrade virtualenvwrapper

# Frontend
