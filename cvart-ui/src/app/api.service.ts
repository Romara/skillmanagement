import { Injectable } from "@angular/core";
import {Http, URLSearchParams, Headers, RequestOptions, Response} from "@angular/http";

const BASE_API_URL = 'http://10.0.10.51:8080/api/';

export default BASE_API_URL


@Injectable()
export class ApiService {
  constructor(private http: Http) {
    let _build = (<any> http)._backend._browserXHR.build;
    (<any> http)._backend._browserXHR.build = () => {
      let _xhr =  _build();
      _xhr.withCredentials = true;
      return _xhr;
    };
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private errorHandle(res: Response) {
    let body = res.json();
    console.log(body.error);
  }

  getRequest(url:string, params:URLSearchParams) {
    return this.http.get(BASE_API_URL + url, { search: params }).toPromise().then(this.extractData).catch(this.errorHandle);
  }

  postRequest(url:string, body:string){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(BASE_API_URL + url, body, options).toPromise().then(this.extractData).catch(this.errorHandle);
  }

  putRequest(url:string, body:string){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(BASE_API_URL + url, body, options).toPromise().then(this.extractData).catch(this.errorHandle);
  }

  deleteRequest(url:string, params:URLSearchParams){
    return this.http.delete(BASE_API_URL + url, params).toPromise().then(this.extractData).catch(this.errorHandle);
  }
}
