import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }  from './app.component';
import { LoginComponent }  from './login/login.component';
import {ProjectListComponent} from "./project/project-list.component";

const routes: Routes = [
  { path: '',  component: ProjectListComponent },
  { path: 'login',  component: LoginComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
