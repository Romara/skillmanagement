import { Component } from '@angular/core';
import {Project} from "./project/project";
import { ProjectService } from "./project/project.service";
import {LoginService} from "./login/login.service";

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: './app.component.html',
})
export class AppComponent  {
  projects: Project[] = [];

  constructor(private loginService:LoginService, private projectService:ProjectService) {
    loginService.login('raman.siamionau', 'G(xAzK&3N4:w')
      .then(()=>{projectService.getProjectsForQuickMenu().then(project=>this.projects=project)});
  }
}
