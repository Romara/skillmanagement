import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule, XSRFStrategy, CookieXSRFStrategy} from "@angular/http";

import { AppComponent }         from './app.component';

import { AppRoutingModule }     from './app-routing.module';
import { LoginComponent } from "./login/login.component";
import { LoginService } from "./login/login.service";
import {ApiService} from "./api.service";
import {ProjectService} from "./project/project.service";
import {EmployeeService} from "./emplolyee/employee.service";
import {ProjectListComponent} from "./project/project-list.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    ProjectListComponent
  ],
  providers: [
    LoginService,
    ApiService,
    ProjectService,
    EmployeeService,
    {
      provide: XSRFStrategy,
      useValue: new CookieXSRFStrategy('csrftoken', 'X-CSRFToken')
    }
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
