import { URLSearchParams, Response } from "@angular/http";
import { ApiService } from "../api.service";
import {Employee} from "./employee";
import { Injectable } from "@angular/core";


@Injectable()
export class EmployeeService {
  constructor(private apiService: ApiService) {}

  private employeeAPI:string = 'employee/';

  getEmployee(id:number):Promise<Employee> {
    let params = new URLSearchParams();
    params.set('id', id.toString());
    return this.apiService.getRequest(this.employeeAPI, params);
  }

  getEmployees(skill_id?:number, project_id?:number):Promise<Employee[]> {
    let params = new URLSearchParams();
    params.set('skill_id', skill_id.toString());
    params.set('project_id', project_id.toString());
    return this.apiService.getRequest(this.employeeAPI, params);
  }
}
