export class Employee {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  date_joined: string;
  profile_id: number;
  first_name_eng: string;
  last_name_eng: string;
  middle_name: string;
  dept_id: number;
  group: number;
  rank: string;
  room: string;
  phone: string;
  skype: string;
  employment_date: string;
  birthday: string;
  image: string;
  is_skill_manager: boolean;
}
