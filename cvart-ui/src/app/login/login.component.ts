import {Component} from '@angular/core';
import {LoginService} from "./login.service";


@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: [ './login.component.css' ]
})
export class LoginComponent  {
  login_cred: string;
  password_cred: string;

  error: string;

  constructor(private login_service: LoginService){
    this.error_handler = this.error_handler.bind(this);
    this.login_success = this.login_success.bind(this);
  }

  login_success(response: any) {
    console.log(response);
  }

  error_handler(response: any) {
    console.log(response);
    this.error = response.json().error;
  }

  login() {
    this.login_service.login(this.login_cred, this.password_cred)
      .then(this.login_success)
      .catch(this.error_handler);
  }
}
