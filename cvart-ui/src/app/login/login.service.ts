import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import BASE_API_URL from "../api.service";


@Injectable()
export class LoginService {
  loginUrl = 'authorize/';

  constructor(private http: Http) {
    let _build = (<any> http)._backend._browserXHR.build;
    (<any> http)._backend._browserXHR.build = () => {
      let _xhr =  _build();
      _xhr.withCredentials = true;
      return _xhr;
    };
  }

  login(login:string, password:string):Promise<any>{
    let body = JSON.stringify({'username':login, 'password':password});
    let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json'});
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    return this.http.post(BASE_API_URL + this.loginUrl, body, options).toPromise();
  }

  logout():Promise<any>{
    return this.http.delete(BASE_API_URL + this.loginUrl).toPromise();
  }
}
