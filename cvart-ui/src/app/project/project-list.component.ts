import {Component} from '@angular/core';
import {Project} from "./project";
import {ProjectService} from "./project.service";


@Component({
  moduleId: module.id,
  selector: 'project_list',
  templateUrl: './project-list.component.html'
})
export class ProjectListComponent  {
  projects: Project[];

  constructor(private project_service: ProjectService){
    project_service.getProjects().then(projects=>this.projects=projects)
  }
}
