import { URLSearchParams, Response } from "@angular/http";
import { ApiService } from "../api.service";
import { Project } from "./project";
import {Injectable} from "@angular/core";


@Injectable()
export class ProjectService {
  constructor(private apiService: ApiService) {}

  private projectAPI:string = 'project/';
  private projectsAPI:string = 'projects/';

  getProject(id:number):Promise<Project> {
    let params = new URLSearchParams();
    params.set('id', id.toString());
    return this.apiService.getRequest(this.projectAPI, params);
  }
  //skill_id?:number, project_id?:number
  getProjects():Promise<Project[]> {
    let params = new URLSearchParams();
    // params.set('skill_id', skill_id.toString());
    // params.set('project_id', project_id.toString());
    return this.apiService.getRequest(this.projectsAPI, params);
  }

  getProjectsForQuickMenu():Promise<Project[]> {
    let params = new URLSearchParams();
    params.set('for_quick_menu', 'True');
    params.set('asd', 'True');
    params.set('dasfas', 'True');
    return this.apiService.getRequest(this.projectsAPI, params);
  }

  createProject(project:Project):Promise<Project> {
    let json = JSON.stringify(project);
    return this.apiService.postRequest(this.projectAPI, json);
  }

  changeProject(project:Project):Promise<Project> {
    let json = JSON.stringify(project);
    return this.apiService.putRequest(this.projectAPI, json);
  }

  deleteProject(project_id:number):Promise<Project> {
    let params = new URLSearchParams();
    params.set('id', project_id.toString());
    return this.apiService.deleteRequest(this.projectAPI, params);
  }
}
