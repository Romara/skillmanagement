export class Project {
  id: number;
  title: string;
  description: string;
  department_id: number;
  customer: number;
  environment: number[];
}
