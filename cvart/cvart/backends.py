from django.contrib.auth import get_user_model

from smg import SMG, SmgIncorrectCredentialsError


class SmgAuthBackend(object):
    def get_user(self, user_id):
        try:
            return get_user_model().objects.get(pk=user_id)
        except get_user_model().DoesNotExist:
            return None

    def authenticate(self, username=None, password=None):
        try:
            smg = SMG()
            smg.login(username, password)
        except SmgIncorrectCredentialsError:
            return None

        try:
            user = get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            return None

        return user
