import datetime
import re


def from_str_unix_to_datetime(c_sharp_unix_string):
    search = re.search(r'\((.\d+)\+', c_sharp_unix_string)
    if search:
        unix_time = search.group(1)
        return from_unix_to_datetime(float(unix_time))

    search = re.search(r'\((.\d+)\+', c_sharp_unix_string)
    if search:
        unix_time = search.group(1)
        return from_unix_to_datetime(float(unix_time))

    raise ValueError('Cannot convert unix string to datetime')


def from_unix_to_datetime(unix):
    return datetime.datetime.fromtimestamp(unix/1000)
