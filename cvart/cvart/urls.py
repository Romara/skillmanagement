from django.conf.urls import url, include

urlpatterns = [
    url(r'api/', include('cvart_app.api.urls'))
]
