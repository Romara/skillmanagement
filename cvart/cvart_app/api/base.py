"""File with basics methods"""
# Rest framework
from rest_framework import status
from rest_framework.response import Response
# Permissions
from cvart_app.api.permission import permission
# Serializers
from cvart_app.api.serializers import *


def handling_request(request, model, many=False, permissions=None, serializer_class=None, query_filter={}):
    """
    GET method - get objects
    :param request: is a request class from requests
    :param model: model class we working on in API
    :param many: for one object or object_query we will work on
    :param permissions: permissions that need to make requests (look cvart_app.api.v1.permission)
    :param serializer_class: serializer class we will use to serialize objects
    :param query_filter: filter django objects with ORM
    :return: Response
    """
    if permissions:
        if permission.login_required in permissions:
            if not request.user.is_authenticated():
                return Response({'error': 'user is not authenticated'}, status=status.HTTP_401_UNAUTHORIZED)

        if permission.department_manager_required in permissions:
            if not request.user.is_departament_manager:
                return Response({'error': 'you do not have permission'}, status=status.HTTP_403_FORBIDDEN)

        if permission.skill_manager_required in permissions:
            if not request.user.is_departament_manager or not request.user.is_skill_manager:
                return Response({'error': 'you do not have permission'}, status=status.HTTP_403_FORBIDDEN)

    if request.method == 'GET':
        return get(request, model, many, serializer_class, query_filter)
    elif request.method == 'POST':
        return post(request, model, serializer_class)
    elif request.method == 'PUT':
        return put(request, model, serializer_class)
    elif request.method == 'DELETE':
        return delete(request, model)


def get(request, model, many, serializer_class, query_filter):
    """
    GET method - get objects
    :param request: is a request class from requests
    :param model: model class we working on in API
    :param many: for one object or object_query we will work on
    :param serializer_class: serializer class we will use to serialize objects
    :param query_filter: filter django objects with ORM
    :return: Response
    """
    if not many:
        model_id = request.GET.get('id', None)
        if model_id:
            try:
                model_object = model.objects.get(id=model_id, **query_filter)
            except model.DoesNotExist:
                return Response({'error': model.__name__ + 'with current id does not exist'},
                                status=status.HTTP_404_NOT_FOUND)
            except ValueError:
                return Response({'error': 'int id required'}, status=status.HTTP_400_BAD_REQUEST)

            if serializer_class:
                serializer = serializer_class(model_object)
            else:
                serializer = eval(model.__name__ + 'Serializer')(model_object)

            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response({'error': 'id required'}, status=status.HTTP_400_BAD_REQUEST)

    model_objects = model.objects.filter(**query_filter)

    if serializer_class:
        serializer = serializer_class(model_objects, many=True)
    else:
        serializer = eval(model.__name__ + 'Serializer')(model_objects, many=True)

    return Response(serializer.data, status=status.HTTP_200_OK)


def post(request, model, serializer_class):
    """
    POST method - creating object
    :param request: is a request class from requests
    :param model: model class we working on in API
    :param serializer_class: serializer class we will use to serialize objects
    :return: Response
    """
    if serializer_class:
        serializer = serializer_class(data=request.data)
    else:
        serializer = eval(model.__name__ + 'Serializer')(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def put(request, model, serializer_class):
    """
    PUT method - change current object
    :param request: is a request class from request
    :param model: model class we working on in API
    :param serializer_class: serializer class we will use to serialize objects
    :return: Response
    """
    object_id = request.data.get('id', None)
    if not object_id:
        return Response({'error': 'id field required'}, status=status.HTTP_400_BAD_REQUEST)

    try:
        model_object = model.objects.get(id=object_id)
    except model.DoesNotExist:
        return Response({'error': model.__name__ + ' does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if serializer_class:
        serializer = serializer_class(model_object, data=request.data)
    else:
        serializer = eval(model.__name__ + 'Serializer')(model_object, data=request.data)

    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def delete(request, model):
    """
    DELETE method - delete object
    :param request: is a request class from request
    :param model: model class we working on in API
    :return: Response
    """
    model_id = request.GET.get('id', None)
    if model_id:
        try:
            model_object = model.objects.get(id=model_id, is_deleted=False)
        except model.DoesNotExist:
            return Response({'error': 'project does not exist'}, status=status.HTTP_404_NOT_FOUND)
        model_object.delete()
        return Response({'status': 'delete successful'})
    return Response({'error': 'id field required'}, status=status.HTTP_400_BAD_REQUEST)
