# Serializer class
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
# Models
from cvart_app.models import Skill


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=128)
    password = serializers.CharField(max_length=128)

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.password = validated_data.get('password', instance.password)
        return instance

    def create(self, validated_data):
        pass


class SkillSerializer(ModelSerializer):
    class Meta:
        model = Skill
        fields = '__all__'
