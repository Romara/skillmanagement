# Django urls
from django.conf.urls import url
# My API
from cvart_app.api.v1.authorize import AuthorizeAPI
from cvart_app.api.v1.employee import EmployeeAPI, EmployeesAPI
from cvart_app.api.v1.customer import CustomerAPI, CustomersAPI
from cvart_app.api.v1.skill import SkillAPI, SkillsAPI
from cvart_app.api.v1.activity import ActivityAPI
from cvart_app.api.v1.position import PositionAPI, PositionsAPI
from cvart_app.api.v1.project import ProjectAPI, ProjectsAPI


urlpatterns = [
    url(r'^authorize/$', AuthorizeAPI.as_view()),
    url(r'^employee/$', EmployeeAPI.as_view()),
    url(r'^employees/$', EmployeesAPI.as_view()),
    url(r'^customer/$', CustomerAPI.as_view()),
    url(r'^customers/$', CustomersAPI.as_view()),
    url(r'^skill/$', SkillAPI.as_view()),
    url(r'^skills/$', SkillsAPI.as_view()),
    url(r'^activity/$', ActivityAPI.as_view()),
    url(r'^position/$', PositionAPI.as_view()),
    url(r'^positions/$', PositionsAPI.as_view()),
    url(r'^project/$', ProjectAPI.as_view()),
    url(r'^projects/$', ProjectsAPI.as_view()),

]
