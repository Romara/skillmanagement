# Restful API
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
# Handling all base requests
from cvart_app.api.base import handling_request
# Models
from cvart_app.models import Activity
# Permissions
from cvart_app.api.permission import permission


class ActivitySerializer(ModelSerializer):
    class Meta:
        model = Activity
        fields = '__all__'


class ActivityAPI(APIView):
    def post(self, request):
        """Create project"""
        return handling_request(request, Activity,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=ActivitySerializer)

    def put(self, request):
        """Change project fields"""
        return handling_request(request, Activity,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=ActivitySerializer)
