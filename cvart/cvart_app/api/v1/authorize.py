# Django authorize
from django.contrib.auth import authenticate, login, logout
# Restful API
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views and serializer
from cvart_app.api.v1.employee import EmployeeSerializer
from cvart_app.api.wrappers import login_required
# Serializer for login
from cvart_app.api.serializers import *


class AuthorizeAPI(APIView):
    @login_required
    def get(self, request):
        """Get method only for get own profile"""
        serializer = EmployeeSerializer(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        """ Login
        Send login and password in json format

        Headers need to set:
        1. Content-Type:"application/json"
        2. For login_required views also attach auth-cookie

        Your request body:
            {
                "username": "UserName",
                "password": "Password"
            }

        Response may be
        1. Status - You can show this message to user or not. Just additional info for sure.
        2. error - Show this message to user. It's important info about what user did wrong.

        Example of response:
            {
                "error": "incorrect username or password"
            }
        """
        serializer = LoginSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(**serializer.data)

        if user is not None:
            if user.is_active:
                login(request, user)
                return Response({'status': 'login success'}, status=status.HTTP_200_OK)
            else:
                return Response({'error': 'user is inactive'}, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({'error': 'incorrect username or password'}, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def delete(self, request):
        """This view logout user"""
        logout(request)
        return Response({'status': 'logout success'}, status=status.HTTP_200_OK)
