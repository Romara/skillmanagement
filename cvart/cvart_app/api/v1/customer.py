# Restful API
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
# Handling all base requests
from cvart_app.api.base import handling_request
# Models
from cvart_app.models import Customer
# Permissions
from cvart_app.api.permission import permission


class CustomerSerializer(ModelSerializer):
    class Meta:
        model = Customer
        exclude = ('created_at', 'updated_at')


class CustomerShortSerializer(ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id', 'first_name', 'last_name', 'company_name')


class CustomerAPI(APIView):
    def get(self, request):
        """Get customer info by id"""
        return handling_request(request, Customer,
                                permissions=[permission.login_required],
                                serializer_class=CustomerSerializer,
                                query_filter={'is_deleted': False})

    def post(self, request):
        """Create customer"""
        return handling_request(request, Customer,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=CustomerSerializer,
                                query_filter={'is_deleted': False})

    def put(self, request):
        """Change customer fields"""
        return handling_request(request, Customer,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=CustomerSerializer,
                                query_filter={'is_deleted': False})

    def delete(self, request):
        """Delete customer with id"""
        return handling_request(request, Customer,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=CustomerSerializer)


class CustomersAPI(APIView):
    def get(self, request):
        customer_first_name = request.GET.get('customer_first_name')
        if customer_first_name:
            return handling_request(request, Customer,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=CustomerShortSerializer,
                                    query_filter={'is_deleted': False,
                                                  'first_name__icontains': customer_first_name})
        else:
            return handling_request(request, Customer,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=CustomerShortSerializer,
                                    query_filter={'is_deleted': False})
