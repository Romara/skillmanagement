# Restful API
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
# Handling all base requests
from cvart_app.api.base import handling_request
# Models
from cvart_app.models import Employee, EmployeeSkill
# Permissions
from cvart_app.api.permission import permission


class EmployeeSerializer(ModelSerializer):
    class Meta:
        model = Employee
        exclude = ('id', 'password', 'groups', 'user_permissions', 'is_staff', 'is_active',
                   'updated_at', 'created_at', 'is_superuser', 'last_login', 'is_deleted')


class EmployeeShortSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = ('profile_id', 'username', 'dept_id', 'group', 'room')


class EmployeeAPI(APIView):
    def get(self, request):
        """Get employee info by id"""
        return handling_request(request, Employee,
                                permissions=[permission.login_required],
                                serializer_class=EmployeeSerializer,
                                query_filter={'is_deleted': False})


class EmployeesAPI(APIView):
    def get(self, request):
        # Find by skill
        skill_id = request.GET.get('skill_id', None)
        if skill_id:
            return handling_request(request, Employee,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=EmployeeShortSerializer,
                                    query_filter={'is_deleted': False,
                                                  'employeeskill__skill_id': skill_id})

        # Find by project_id
        project_id = request.GET.get('project_id', None)
        if project_id:
            return handling_request(request, Employee,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=EmployeeShortSerializer,
                                    query_filter={'is_deleted': False,
                                                  'activity__position__project_id': project_id})

        # Find by First name
        name = request.GET.get('name', None)
        if name:
            return handling_request(request, Employee,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=EmployeeShortSerializer,
                                    query_filter={'is_deleted': False,
                                                  })

        return handling_request(request, Employee,
                                many=True,
                                permissions=[permission.login_required],
                                serializer_class=EmployeeShortSerializer,
                                query_filter={'is_deleted': False})
