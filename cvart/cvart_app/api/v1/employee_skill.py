# Restful API
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# Handling all base requests
from cvart_app.api.base import handling_request
# Permissions
from cvart_app.api.permission import permission
# Models
from cvart_app.models import EmployeeSkill
from cvart_app.api.serializers import SkillSerializer


class EmployeeSkillSerializer(ModelSerializer):
    skill = SkillSerializer

    class Meta:
        model = EmployeeSkill
        fields = '__all__'


class EmployeeSkillAPI(APIView):
    def post(self, request):
        """Create project"""
        return handling_request(request, EmployeeSkill,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=EmployeeSkillSerializer)

    def put(self, request):
        """Change project fields"""
        return handling_request(request, EmployeeSkill,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=EmployeeSkillSerializer)

    def delete(self, request):
        """Delete project with id"""
        return handling_request(request, EmployeeSkill,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=EmployeeSkillSerializer)


class EmployeeSkillsAPI(APIView):
    def get(self, request):
        """Get all employee skills by employee id"""
        employee_id = request.GET.get('employee_id', None)
        if employee_id:
            return handling_request(request, EmployeeSkill,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=EmployeeSkillSerializer,
                                    query_filter={'employee_id': employee_id})
        return Response({'error': 'employee_id required'}, status=status.HTTP_400_BAD_REQUEST)
