# Restful API
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView, Response
from rest_framework import status
# Handling all base requests
from cvart_app.api.base import handling_request
# Models
from cvart_app.models import Position
# Permissions
from cvart_app.api.permission import permission
# Serializers
from cvart_app.api.serializers import SkillSerializer


class PositionSerializer(ModelSerializer):

    class Meta:
        model = Position
        exclude = ('created_at', 'updated_at')


class PositionAPI(APIView):
    def get(self, request):
        """Get position info by id"""
        return handling_request(request, Position,
                                permissions=[permission.login_required],
                                serializer_class=PositionSerializer,
                                query_filter={'is_deleted': False})

    def post(self, request):
        """Create position"""
        return handling_request(request, Position,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=PositionSerializer)

    def put(self, request):
        """Change position fields"""
        return handling_request(request, Position,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=PositionSerializer)

    def delete(self, request):
        """Delete position with id"""
        return handling_request(request, Position,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=PositionSerializer)


class PositionsAPI(APIView):
    def get(self, request):
        project_id = request.GET.get('project_id', None)
        if project_id:
            return handling_request(request, Position,
                                    many=True,
                                    permissions=[permission.login_required],
                                    serializer_class=PositionSerializer,
                                    query_filter={'is_deleted': False,
                                                  'project_id': project_id})
        return Response({'error': 'project_id required'}, status=status.HTTP_400_BAD_REQUEST)
