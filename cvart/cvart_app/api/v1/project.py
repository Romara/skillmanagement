# Restful API
from rest_framework.serializers import ModelSerializer
from rest_framework.views import APIView
# Handling all base requests
from cvart_app.api.base import handling_request
# Models
from cvart_app.models import Project
# Permissions
from cvart_app.api.permission import permission
# Serializer
from cvart_app.api.serializers import SkillSerializer


class ProjectSerializer(ModelSerializer):

    class Meta:
        model = Project
        exclude = ('is_deleted', 'created_at', 'updated_at')


class ProjectShortSerializer(ModelSerializer):
    class Meta:
        model = Project
        fields = ('id', 'title')


class ProjectAPI(APIView):
    def get(self, request):
        """Get project info by id"""
        return handling_request(request, Project,
                                permissions=[permission.login_required],
                                serializer_class=ProjectSerializer,
                                query_filter={'is_deleted': False})

    def post(self, request):
        """Create project"""
        return handling_request(request, Project,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=ProjectSerializer)

    def put(self, request):
        """Change project fields"""
        return handling_request(request, Project,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=ProjectSerializer)

    def delete(self, request):
        """Delete project with id"""
        return handling_request(request, Project,
                                permissions=[permission.login_required, permission.department_manager_required],
                                serializer_class=ProjectSerializer)


class ProjectsAPI(APIView):
    def get(self, request):
        """Get all projects"""
        query_filter = {'is_deleted': False}

        for_quick_menu = request.GET.get('for_quick_menu', None)
        if for_quick_menu:
            query_filter['department_id'] = request.user.employee.dept_id

        project_title = request.GET.get('title', None)
        if project_title:
            query_filter['title__icontains'] = project_title

        project_department_id = request.GET.get('department_id', None)
        if project_department_id:
            query_filter['department_id'] = project_department_id

        return handling_request(request, Project,
                                many=True,
                                permissions=[permission.login_required],
                                serializer_class=ProjectShortSerializer,
                                query_filter=query_filter)
