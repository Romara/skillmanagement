# Restful API
from rest_framework.views import APIView
# Handling all base requests
from cvart_app.api.base import handling_request
# Permissions
from cvart_app.api.permission import permission
# Models
from cvart_app.models import Skill
# Serializer
from cvart_app.api.serializers import SkillSerializer


class SkillAPI(APIView):
    def post(self, request):
        return handling_request(request, Skill,
                                permissions=[permission.skill_manager_required],
                                serializer_class=SkillSerializer)

    def put(self, request):
        return handling_request(request, Skill,
                                permissions=[permission.skill_manager_required],
                                serializer_class=SkillSerializer)


class SkillsAPI(APIView):
    def get(self, request):
        query_filter = {}

        skill_title = request.GET.get('title', None)
        if skill_title:
            query_filter['title_icontains'] = skill_title

        return handling_request(request, Skill,
                                many=True,
                                permissions=[permission.skill_manager_required],
                                serializer_class=SkillSerializer,
                                query_filter=query_filter)
