from django.contrib.auth.models import User
from django.db import models


class Customer(models.Model):
    first_name = models.CharField(max_length=128)
    last_name = models.CharField(max_length=128, blank=True, null=True)
    skype = models.CharField(max_length=128, blank=True, null=True)
    phone = models.CharField(max_length=128, blank=True, null=True)
    contact_info = models.CharField(max_length=256, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    company_name = models.CharField(max_length=128, blank=True, null=True)

    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def delete(self, using=None, keep_parents=False):
        self.is_deleted = True

    def __str__(self):
        return '{} {}'.format(self.first_name if self.first_name else '', self.last_name if self.last_name else '')


class Skill(models.Model):
    title = models.CharField(max_length=128, unique=True)
    url_title = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return '{}'.format(self.title)


class Project(models.Model):
    title = models.CharField(max_length=256, unique=True)
    description = models.TextField(blank=True, null=True)
    environment = models.ManyToManyField(Skill)
    department_id = models.IntegerField()
    customer = models.ForeignKey(Customer)

    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}. Status: {}'.format(self.title, 'active' if not self.finished else 'finished')


class Position(models.Model):
    title = models.CharField(max_length=128)
    responsibilities = models.TextField(blank=True, null=True)
    skills = models.ManyToManyField(Skill)

    project = models.ForeignKey(Project)

    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}. Status: {}'.format(self.title, 'active' if not self.finished else 'finished')


class Employee(User):
    profile_id = models.IntegerField(unique=True)

    first_name_eng = models.CharField(max_length=128, null=True, blank=True)
    last_name_eng = models.CharField(max_length=128, null=True, blank=True)

    middle_name = models.CharField(max_length=128, null=True, blank=True)

    dept_id = models.IntegerField(null=True, blank=True)
    group = models.CharField(max_length=64, null=True, blank=True)
    rank = models.CharField(max_length=128, null=True, blank=True)
    room = models.CharField(max_length=64, null=True, blank=True)

    phone = models.CharField(max_length=64, null=True, blank=True)
    skype = models.CharField(max_length=128, null=True, blank=True)

    employment_date = models.DateField(null=True, blank=True)
    birthday = models.DateField(null=True, blank=True)
    image = models.CharField(max_length=128, null=True, blank=True)

    is_skill_manager = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{} {}, profile_id: {}'.format(self.first_name_eng, self.last_name_eng, self.profile_id)

    @property
    def is_departament_manager(self):
        if 'Department Manager' in self.rank or 'Group Manager' in self.rank:
            return True
        return False


class EmployeeSkill(models.Model):
    employee = models.ForeignKey(Employee)
    skill = models.ForeignKey(Skill)

    level = models.IntegerField(default=0)

    class Meta:
        unique_together = ('employee', 'skill')


class Activity(models.Model):
    position = models.ForeignKey(Position)
    employee = models.ForeignKey(Employee)

    date_start = models.DateField()
    date_end = models.DateField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'from {} to {}. Employee "{}" on "{}" position'.format(self.date_start, self.date_end,
                                                                      self.employee, self.position)
