import requests
import datetime

from cvart.common import from_str_unix_to_datetime

from cvart_app.models import Employee


class SmgIncorrectCredentialsError(Exception):
    pass


class SmgConnectionError(Exception):
    pass


class SmgError(Exception):
    pass


class SMG:
    # API Reference
    # https://smg.itechart-group.com/MobileServiceNew/MobileService.svc/help
    ENDPOINTS = {
        'auth': 'https://smg.itechart-group.com/MobileServiceNew/MobileService.svc/Authenticate',
        'get_profiles': 'https://smg.itechart-group.com/MobileServiceNew/MobileService.svc/GetDetailedEmployeesByDeptId',
    }

    def __init__(self):
        self.session_id = 0
        self.employee = None

    def login(self, username='raman.siamionau', password='G(xAzK&3N4:w'):
        auth_url = self.ENDPOINTS['auth']
        params = {'username': username, 'password': password}
        response = requests.get(auth_url, params=params)
        if response.ok:
            response_json = response.json()
            if response_json['ErrorCode']:
                raise SmgIncorrectCredentialsError(response)
            else:
                self.session_id = response_json['SessionId']
        else:
            raise SmgConnectionError(response)

    def update_all_profiles(self):
        self.login()

        get_profiles_url = self.ENDPOINTS['get_profiles']
        params = {'departmentId': 0, 'sessionId': self.session_id}
        response = requests.get(get_profiles_url, params=params)
        if response.ok:
            response_json = response.json()
            if response_json['ErrorCode']:
                raise SmgError(response)
            else:
                # Update all
                profiles = response_json['Profiles']
                for profile in profiles:
                    Employee.objects.update_or_create(username=profile['DomenName'],
                                                      defaults={
                                                          'first_name': profile['FirstName'],
                                                          'last_name': profile['LastName'],
                                                          'email': profile['Email'],
                                                          'profile_id': profile['ProfileId'],
                                                          'birthday':  from_str_unix_to_datetime(profile['Birthday']),
                                                          'dept_id': profile['DeptId'],
                                                          'employment_date': from_str_unix_to_datetime(profile['EmploymentDate']),
                                                          'first_name_eng': profile['FirstNameEng'],
                                                          'group': profile['Group'],
                                                          'last_name_eng': profile['LastNameEng'],
                                                          'middle_name': profile['MiddleName'],
                                                          'phone': profile['Phone'],
                                                          'rank': profile['Rank'],
                                                          'room': profile['Room'],
                                                          'image': profile['Image'],
                                                          'skype': profile['Skype'],
                                                          'is_deleted': False
                                                      })

                for employee in Employee.objects.filter(updated_at__lte=datetime.date.today()):
                    employee.is_deleted = True
                    employee.save()

        else:
            raise SmgConnectionError(response)
